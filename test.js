const puppeteer = require('puppeteer');

(async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.goto('https://lucapec.gitlab.io/curso-devops/');
  // Agrega tus pruebas de Puppeteer aquí
  await browser.close();
})();