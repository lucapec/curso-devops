# Etapa de compilación
FROM node:14 as build
WORKDIR /app
COPY package.json package-lock.json /app/
RUN npm install
COPY . /app
RUN npm run build

# Imagen final
FROM ubuntu:latest
# Copiar los archivos necesarios de la etapa de compilación
COPY --from=build /app /app
# Instalar Node.js y npm en la imagen de Ubuntu
RUN apt-get update && apt-get install -y nodejs npm
# Establecer el directorio de trabajo y ejecutar la aplicación
WORKDIR /app
CMD ["npm", "start"]
